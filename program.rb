def validate(key)
  raise ArgumentError, 'no an Integer' unless key.is_a?(Integer)

  return false unless key.to_s.length == 16

  return false unless (51..55).include?(key.to_s[0..1].to_i)

  key_array = key.to_s.chars.map(&:to_i)

  multiplied_key_array =
    key_array.each_with_index.map do |element, index|
      index.even? ? element * 2 : element
    end
  final_sum = multiplied_key_array.join.chars.map(&:to_i).reduce(&:+)

  (final_sum % 10).zero?
end
